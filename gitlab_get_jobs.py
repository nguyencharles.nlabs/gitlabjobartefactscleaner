import sys
import pycurl
import re
import colorama
from colorama import Fore, Style

import parameters

URL = "https://" + parameters.server + "/api/v4/projects/" + "%s"%parameters.project_id + "/jobs"
FILTER = "scope[]=success&scope[]=failed&scope[]=canceled"
PAGINATION = "pagination=keyset&per_page=100&order_id=id&sort=asc&page=%s"%parameters.init_page 

headers = {}

def header_parser(header_line):
  header_line = header_line.decode('iso-8859-1')
  if ':' not in header_line:
    return
  name, value = header_line.split(':', 1)
  name = name.strip()   # Remove whitespace
  value = value.strip() # Remove whitespace
  name = name.lower()
  headers[name] = value

def curl_exec(url, file):
  c = pycurl.Curl()
  c.setopt(c.URL, url)
  c.setopt(pycurl.FOLLOWLOCATION, True) # Follow redirection
  c.setopt(pycurl.HTTPHEADER, ["PRIVATE-TOKEN:"+parameters.token])  # Set Header
  c.setopt(c.WRITEDATA, file) # Set output file
  c.setopt(c.HEADERFUNCTION, header_parser) # Set header parser
  c.perform()
  response_code = c.getinfo(c.RESPONSE_CODE)
  c.close()
  return response_code

def get_next_url():
  # Format the link value from the header to retrieve the linked page url including potentially next, prev and first
  if 'link' in headers:
    links = dict((re.split('"', b.strip())[1], re.split('[<>]', a.strip())[1])
      for a, b in (elem.split(';') for elem in headers['link'].split(','))
    )
    if 'next' in links:
      return links['next']
  return 0

def main() -> int:
  url = URL+'?'+FILTER+'&'+PAGINATION
  page = parameters.init_page
  
  colorama.init()

  while True:
    retry = 0
    while True:
      print(Style.RESET_ALL)

      # Generate jobs-page json files
      print(url)
      file = open('project-'+"%s"%parameters.project_id+'-jobs-page' + "%s"%page + ".json",'wb')
      response_code = curl_exec(url, file)
      file.close()

      # Manage HTTP success code
      if response_code == 200:
        print(Fore.GREEN + 'HTTP status code: %d' % response_code)
        break
      print(Fore.YELLOW + 'HTTP status code: %d' % response_code)
      
      # Retry
      print(Fore.YELLOW + "Error: HTTP status code != 200")
      retry += 1
      if(retry > parameters.retry):
        print(Fore.RED + "End of query: error HTTP status code != 200")
        return -1

      print(Fore.YELLOW + "Retry %d/%d..."%(retry, parameters.retry))
        
    # Get next URL in the links metadata
    url = get_next_url()
    if url == 0:
      print(Fore.GREEN + "End of query: no next link in the header")
      break
    page += 1

  return 0

if __name__ == '__main__':
  sys.exit(main())