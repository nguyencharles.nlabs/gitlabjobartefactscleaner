# GitLab Job Artefacts Cleaner

## /!\ Warning /!\

This software has been developed to delete ALL the artifacts with archive type produced by the pipeline jobs in ONE defined project.
Be careful, any bad move can delete DEFINITIVELY your data on GitLab.

## Requirements

All the API calls are done with pycurl-7.45.1.
Check the installation process [here](http://pycurl.io/docs/latest/install.html).

## Usage

Set your parameters in parameters.py
```
# General configs
project_id = "<GITLAB-PROJECT-ID>"
token = "<YOUR-TOKEN>"
server = "gitlab.com"             # Or the URL of your own instance of GitLab
retry = 5                         # Retry times if HTTP CODE != 200

# For get_jobs and get_artifacts_from_jobs
init_page = 1                     # The entry page to begin browsing the jobs (Default: 1)

# For get_artifacts_from_jobs and get_artifacts_from_jobs_id
get_artifacts_with_url = False    # Keep the job url
get_artifacts_with_date = False   # Keep the created_at and artifacts_expire_at dates

# For get_artifacts_from_jobs_id
job_id_desc_start = 10000         # Job ID to begin with in desc order (ex. 10000)
job_id_desc_end = 1               # Job ID to end with in desc order (Default: 1)

# For delete_artifacts
min_size_file = 0                 # Size of the files to be deleted, in Byte (Default: 0)

# For delete pipelines
delete_before_date = "2021-10-20" # Delete pipelines before that date (ISO format: YYYY-MM-DD)
```

Start the scripts

To delete artifacts:
```
# This first script will generate the list of all the jobs of your project paginated into a json file by group of 100 jobs.
py gitlab_get_jobs.py

# The second script will parse the previous json file to find the artifacts with the "archive" type and then generate another json file with all the artifacts.
py gitlab_get_artifacts_from_jobs_jsonfiles.py

# OR

# If you want to check the artifacts directly with the job IDs
py gitlab_get_artifacts_from_job_IDs_directly.py

# Then

# The third script will list then delete all the artifacts listed in the last file.
py gitlab_delete_artifacts.py
```

To delete the pipelines completely:
```
# This first script will generate the list of all the pipelines of your project paginated into a json file by group of 100 pipelines.
py gitlab_get_pipelines.py

# This script will list then delete the pipelines listed.
py gitlab_delete_pipelines.py
```
