import os, sys
import pycurl
import json
import colorama
from colorama import Fore, Style

import parameters

URL = "https://" + parameters.server + "/api/v4/projects/" + "%s"%parameters.project_id + "/jobs"
inputfile = 'project-'+"%s"%parameters.project_id+'-artifacts.json'

artifacts_to_be_deleted = []
stat = {
  "deleted": {
    "count": 0,
    "size": 0
  },
  "notfound":
  {
    "count": 0,
    "size": 0
  },
  "error": {
    "count": 0,
    "size": 0
  }
}

def warning_startup():
  while True:
    print(Style.RESET_ALL)
    print(Fore.YELLOW + "/!\ Careful /!\ this script will delete the job artefacts from the project %d"%parameters.project_id+'.')
    i_str = input("There is a risk of data loss, you know what you are doing, [yes] to continue: ")
    if i_str == 'yes':
      return 0
    else:
      return -1
      
def warning_before_delete():
  while True:
    print(Style.RESET_ALL)
    print(Fore.YELLOW + "/!\ Careful /!\ THE ARTIFACTS LISTED ABOVE WILL BE DELETED, ARE YOU SURE ??")
    i_str = input("There is a risk of data loss, you know what you are doing, [yes] to continue: ")
    if i_str == 'yes':
      return 0
    else:
      return -1
  
def sizeof_fmt(num, suffix="B"):
  for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
    if abs(num) < 1024.0:
      return f"{num:3.1f}{unit}{suffix}"
    num /= 1024.0
  return f"{num:.1f}Yi{suffix}"


def list_from_artifactsjsonfile(file):
  data = json.load(file)
  total_count_artifacts = len(data)
  print(Style.RESET_ALL)
  print("-----------------------------------------------------------------")
  print("Project %s"%parameters.project_id)
  print("-----------------------------------------------------------------")
  print("          {:<15} {:<12} {:<9} {:<20}".format('Job ID','Size','Type','Name'))
  print("-----------------------------------------------------------------")
  for i in range(total_count_artifacts):
    artifact_size = data[i]['artifact']['size']
    if artifact_size >= parameters.min_size_file:
      artifact = {}
      artifact['id'] = int(data[i]['id'])
      artifact['size'] = artifact_size
      artifact['format'] = data[i]['artifact']['file_type']
      artifact['name'] = data[i]['artifact']['filename']
      artifacts_to_be_deleted.append(artifact)
      print("{:<5} | {:<13} | {:<10} | {:<5} | {:<15}".format("%d"%i+"/%d"%(total_count_artifacts-1), artifact['id'], sizeof_fmt(artifact['size']), artifact['format'], artifact['name']))
  print("-----------------------------------------------------------------")
  print(Fore.YELLOW + "Total      | {:<10} | {:<8} files".format(sizeof_fmt(sum(j.get('size', 0) for j in artifacts_to_be_deleted)), len(artifacts_to_be_deleted)))
  return 0

def curl_exec(url, bool_delete):
  c = pycurl.Curl()
  c.setopt(c.URL, url)
  c.setopt(pycurl.FOLLOWLOCATION, True) # Follow redirection
  c.setopt(pycurl.HTTPHEADER, ["PRIVATE-TOKEN:"+parameters.token])  # Set Header
  c.setopt(c.NOBODY, True)  # Get only Header
  if bool_delete:
    c.setopt(pycurl.CUSTOMREQUEST, "DELETE")
  c.perform()
  response_code = c.getinfo(c.RESPONSE_CODE)
  c.close()
  return response_code

def delete_artifacts():
  global stat

  print(Style.RESET_ALL)
  print("-----------------------------------------------------------------")
  print(Fore.YELLOW + "Project %s"%parameters.project_id + Style.RESET_ALL)
  print("-----------------------------------------------------------------")
  print("           {:<15} {:<12} {:<9} {:<20}".format('Job ID','Size','Status','Name'))
  print("-----------------------------------------------------------------")

  total_count_artifacts = len(artifacts_to_be_deleted)
  
  # Send a delete request through gitlab API
  for i in range(total_count_artifacts):
    url = URL+"/%s"%artifacts_to_be_deleted[i]['id']+'/artifacts'
    
    # Send a delete request through gitlab API
    response_code = curl_exec(url, False)

    # Artifact not found
    if response_code == 404:
      stat["notfound"]["count"] += 1
      stat["notfound"]["size"] += artifacts_to_be_deleted[i]['size']
      print(Fore.YELLOW + "{:<5} | {:<13} | {:<10} | not found | {:<15}".format("%d"%(i+1)+"/%d"%(total_count_artifacts), artifacts_to_be_deleted[i]['id'], sizeof_fmt(artifacts_to_be_deleted[i]['size']), artifacts_to_be_deleted[i]['name']))
      continue

    # Artifact exists
    elif response_code == 200:

      # Try to delete the artifact
      response_code = curl_exec(url, True)

      # If deletion succeeded, will receive 204 No Content code
      if response_code == 204:
        stat["deleted"]["count"] += 1
        stat["deleted"]["size"] += artifacts_to_be_deleted[i]['size']
        print(Fore.GREEN + "{:<5} | {:<13} | {:<10} | deleted | {:<15}".format("%d"%(i+1)+"/%d"%(total_count_artifacts), artifacts_to_be_deleted[i]['id'], sizeof_fmt(artifacts_to_be_deleted[i]['size']), artifacts_to_be_deleted[i]['name']))
        continue

    # Error case
    stat["error"]["count"] += 1
    stat["error"]["size"] += artifacts_to_be_deleted[i]['size']
    print(Fore.RED + "{:<5} | {:<13} | {:<10} | error   | {:<15}".format("%d"%(i+1)+"/%d"%(total_count_artifacts), artifacts_to_be_deleted[i]['id'], sizeof_fmt(artifacts_to_be_deleted[i]['size']), artifacts_to_be_deleted[i]['name']))
  
  return 0

def main() -> int:
  colorama.init()

  if os.path.isfile(inputfile):
    if (warning_startup() != 0):
      return 0
    
    # List all the artifacts from the artifacts.json previously generated
    file = open(inputfile,'r')
    list_from_artifactsjsonfile(file)
    file.close()
    if len(artifacts_to_be_deleted) == 0:
      print(Fore.YELLOW + "no artifact to delete.")
      return 0

    if (warning_startup() != 0):
      return 0
    
    # Delete all artifacts listed
    if (delete_artifacts() != 0):
      return -1
    
    print(Style.RESET_ALL + "-----------------------------------------------------------------")
    print(Fore.GREEN +  "Total deleted   | {:<10} | {:<8} files".format(sizeof_fmt(stat["deleted"]["size"]), stat["deleted"]["count"]))
    print(Fore.YELLOW + "Total not found | {:<10} | {:<8} files".format(sizeof_fmt(stat["notfound"]["size"]), stat["notfound"]["count"]))
    print(Fore.RED +    "Total errors    | {:<10} | {:<8} files".format(sizeof_fmt(stat["error"]["size"]), stat["error"]["count"]))
    print(Fore.GREEN + "Done !")
    print(Style.RESET_ALL)

  else:
    print(Fore.RED + "no file: "+inputfile)
  return 0

if __name__ == '__main__':
  sys.exit(main())