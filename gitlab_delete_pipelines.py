import os, sys
import pycurl
import json
import colorama
from colorama import Fore, Style
from datetime import datetime

import parameters

URL = "https://" + parameters.server + "/api/v4/projects/" + "%s"%parameters.project_id + "/pipelines"

pipelines_to_be_deleted = []
stat = {
  "deleted": {
    "count": 0
  },
  "notfound":
  {
    "count": 0
  },
  "error": {
    "count": 0
  }
}

def warning_startup(date):
  while True:
    print(Style.RESET_ALL)
    print(Fore.YELLOW + "/!\ Careful /!\ this script will delete the pipelines from the project %d "%parameters.project_id)
    print(Fore.YELLOW + "older than %s"%date.strftime("%d %B %Y")+'.')
    i_str = input("There is a risk of data loss, you know what you are doing, [yes] to continue: ")
    if i_str == 'yes':
      return 0
    else:
      return -1
      
def warning_before_delete():
  while True:
    print(Style.RESET_ALL)
    print(Fore.YELLOW + "/!\ Careful /!\ THE PIPELINES LISTED ABOVE WILL BE DELETED, ARE YOU SURE ??")
    i_str = input("There is a risk of data loss, you know what you are doing, [yes] to continue: ")
    if i_str == 'yes':
      return 0
    else:
      return -1
  
def sizeof_fmt(num, suffix="B"):
  for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
    if abs(num) < 1024.0:
      return f"{num:3.1f}{unit}{suffix}"
    num /= 1024.0
  return f"{num:.1f}Yi{suffix}"

def parse_pipelinejsonfile(file, date):
  global pipelines_to_be_deleted

  data = json.load(file)
  # print(json.dumps(data, indent=4))

  for i in range(len(data)):
    pipeline_data = {}
    # Check if data is an array (or str)

    if isinstance(data, list) and 'id' in data[i] and datetime.strptime(data[i].get('created_at'), "%Y-%m-%dT%H:%M:%S.%fZ") < date:
      pipeline_data['id'] = data[i].get('id')
      pipeline_data['status'] = data[i].get('status')
      pipeline_data['created_at'] = data[i].get('created_at')
      pipeline_data['updated_at'] = data[i].get('updated_at')
      pipeline_data['url'] = data[i].get('web_url')
      pipelines_to_be_deleted.append(pipeline_data)
      print("{:<5} | {:<13} | {:<10} | {:<5} | {:<15}".format(
        "%d"%(len(pipelines_to_be_deleted)),
        pipeline_data['id'],
        pipeline_data['status'],
        pipeline_data['created_at'],
        pipeline_data['updated_at']
      ))
  return 0

def curl_exec(url, bool_delete):
  c = pycurl.Curl()
  c.setopt(c.URL, url)
  c.setopt(pycurl.FOLLOWLOCATION, True) # Follow redirection
  c.setopt(pycurl.HTTPHEADER, ["PRIVATE-TOKEN:"+parameters.token])  # Set Header
  c.setopt(c.NOBODY, True)  # Get only Header
  if bool_delete:
    c.setopt(pycurl.CUSTOMREQUEST, "DELETE")
  c.perform()
  response_code = c.getinfo(c.RESPONSE_CODE)
  c.close()
  return response_code

def delete_pipelines():
  global stat

  print(Style.RESET_ALL)
  print("---------------------------------------------------------------------------")
  print(Fore.YELLOW + "Project %s"%parameters.project_id + Style.RESET_ALL)
  print("---------------------------------------------------------------------------")
  print("        {:<15} {:<8} {:<25}".format('Job ID','Status','Created_at'))
  print("---------------------------------------------------------------------------")

  # Send a delete request through gitlab API
  for i in range(len(pipelines_to_be_deleted)):
    url = URL+"/%s"%pipelines_to_be_deleted[i]['id']

    # Send a delete request through gitlab API
    response_code = curl_exec(url, False)
    text_color = Fore.GREEN

    # Artifact exists
    if response_code == 200:
      # Try to delete the artifact
      response_code = curl_exec(url, True)
      # If deletion succeeded, will receive 204 No Content code
      if response_code == 204:
        stat["deleted"]["count"] += 1
      else:
        text_color = Fore.RED
        
    # Artifact not found
    elif response_code == 404:
      text_color = Fore.YELLOW
      stat["notfound"]["count"] += 1

    # Error case
    else:
      text_color = Fore.RED
      stat["error"]["count"] += 1

    print(text_color + "{:<5} | {:<13} | {:<3} | {:<15}".format(
      "%d"%(i+1)+"/%d"%(len(pipelines_to_be_deleted)),
      pipelines_to_be_deleted[i]['id'],
      response_code,
      pipelines_to_be_deleted[i]['created_at']
    ))
  
  return 0

def main() -> int:
  colorama.init()

  print(Style.RESET_ALL)
  print("---------------------------------------------------------------------------")
  print("Project %s"%parameters.project_id)
  print("---------------------------------------------------------------------------")
  print("        {:<15} {:<15} {:<25} {:<25}".format('Pipeline ID','Status','Created_at','Updated_at'))
  print("---------------------------------------------------------------------------")

  # Parse date
  delete_date = datetime.fromisoformat(parameters.delete_before_date)

  # Iterate for each project-<project_number>-pipelines-page-<page_number>.json
  page = parameters.init_page
  while True:

    # Parsing jobs json files
    pipelinejsonfile = 'project-'+"%s"%parameters.project_id+'-pipelines-page' + "%s"%page + '.json'
    if os.path.isfile(pipelinejsonfile):
      print(pipelinejsonfile)
      file = open(pipelinejsonfile,'r', encoding="utf8")
      parse_pipelinejsonfile(file, delete_date)   # Parsing
      file.close()

    else:
      print(Fore.YELLOW + "no file: "+pipelinejsonfile)
      break
    page +=1

  print("---------------------------------------------------------------------------")
  print(Fore.YELLOW + "Total pipelines      | {:<4} pipelines".format(len(pipelines_to_be_deleted)))
  
  if (len(pipelines_to_be_deleted) == 0):
    return 0

  if (warning_startup(delete_date) != 0):
    return 0
    
  # Delete all pipelines listed
  if (delete_pipelines() != 0):
    return -1
  
  print(Style.RESET_ALL + "---------------------------------------------------------------------------")
  print(Fore.GREEN +  "Total deleted   | {:<8} pipelines".format(stat["deleted"]["count"]))
  print(Fore.YELLOW + "Total not found | {:<8} pipelines".format(stat["notfound"]["count"]))
  print(Fore.RED +    "Total errors    | {:<8} pipelines".format(stat["error"]["count"]))
  print(Fore.GREEN + "Done !")
  print(Style.RESET_ALL)

  return 0

if __name__ == '__main__':
  sys.exit(main())