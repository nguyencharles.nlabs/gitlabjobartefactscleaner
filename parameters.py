# General configs
project_id = "<GITLAB-PROJECT-ID>"
token = "<YOUR-TOKEN>"
server = "gitlab.com"             # Or the URL of your own instance of GitLab
retry = 5                         # Retry times if HTTP CODE != 200

# For get_jobs and get_artifacts_from_jobs
init_page = 1                     # The entry page to begin browsing the jobs (Default: 1)

# For get_artifacts_from_jobs and get_artifacts_from_jobs_id
get_artifacts_with_url = False    # Keep the job url
get_artifacts_with_date = False   # Keep the created_at and artifacts_expire_at dates

# For get_artifacts_from_jobs_id
job_id_desc_start = 10000         # Job ID to begin with in desc order (ex. 10000)
job_id_desc_end = 1               # Job ID to end with in desc order (Default: 1)

# For delete_artifacts
min_size_file = 0                 # Size of the files to be deleted, in Byte (Default: 0)

# For delete pipelines
delete_before_date = "2021-10-20"