import os, sys
import pycurl
from io import BytesIO
import json
import colorama
from colorama import Fore, Style

import parameters

URL = "https://" + parameters.server + "/api/v4/projects/" + "%s"%parameters.project_id + "/jobs/"

jobs_count = 0
artifacts_count = 0
total_artifacts_size = 0
outputfile = 'project-'+"%s"%parameters.project_id+'-artifacts.json'

def check_if_result_exist():
  if os.path.isfile(outputfile):
    print(Fore.RED + "Error: " + outputfile + " already exist")
    return -1
  return 0

def close_file():
  ofile = open(outputfile,'a', encoding="utf8")
  ofile.write(']')
  ofile.close()
  return 0

def keep_result(result):
  global artifacts_count
  ofile = open(outputfile,'a', encoding="utf8")
  if artifacts_count==0:
    ofile.write('[')
  else:
    ofile.write(", ")
  ofile.write(json.dumps(result))
  ofile.close()
  return 0

def parse_data(buffer):
  global jobs_count
  global artifacts_count
  global total_artifacts_size

  data = json.loads(buffer.getvalue().decode('iso-8859-1'))
  # print(json.dumps(data, indent=4))

  # Check if data is a dict
  if isinstance(data, dict):
    jobs_count += 1
    
    print("{:<5} | {:<13}".format("%d"%artifacts_count, data.get('id')))

    artifact_data = data.get('artifacts')  
    if artifact_data != []:
      for j in range(len(artifact_data)):
        
        # Keep only artifact with the archive file_type
        if artifact_data[j].get('file_type') == "archive":
          artifact = {}
          artifact['id'] = data.get('id')
          if parameters.get_artifacts_with_url:
            artifact['url'] = data.get('web_url')
          if parameters.get_artifacts_with_date:
            artifact['date_job_creation'] = data.get('created_at')
            artifact['date_artifact_expiration'] = data.get('artifacts_expire_at')
          artifact['artifact'] = artifact_data[j]
          
          # Writing in file
          keep_result(artifact)

          # Stat and display
          total_artifacts_size += artifact_data[j]['size']
          artifacts_count+=1
          print("{:<5} | {:<13} | {:<10} | {:<5} | {:<15}".format("%d"%artifacts_count, artifact['id'], sizeof_fmt(artifact_data[j]['size']), artifact_data[j]['file_type'], artifact_data[j]['filename']))
  return 0

def curl_exec(url, buffer):
  c = pycurl.Curl()
  c.setopt(c.URL, url)
  c.setopt(pycurl.FOLLOWLOCATION, True) # Follow redirection
  c.setopt(pycurl.HTTPHEADER, ["PRIVATE-TOKEN:"+parameters.token])  # Set Header
  c.setopt(c.WRITEDATA, buffer) # Set buffer
  c.perform()
  response_code = c.getinfo(c.RESPONSE_CODE)
  c.close()
  return response_code

def sizeof_fmt(num, suffix="B"):
  for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
    if abs(num) < 1024.0:
      return f"{num:3.1f}{unit}{suffix}"
    num /= 1024.0
  return f"{num:.1f}Yi{suffix}"

def main() -> int:
  colorama.init()
  
  if check_if_result_exist() != 0:
    return -1

  print(Style.RESET_ALL)
  print("-----------------------------------------------------------------")
  print("Project %s"%parameters.project_id)
  print("-----------------------------------------------------------------")
  print("          {:<15} {:<10} {:<9} {:<20}".format('Job ID','Size','Type','Name'))
  print("-----------------------------------------------------------------")

  # Iterate for each jobs
  job_id = parameters.job_id_desc_start

  while job_id >= parameters.job_id_desc_end:
    while True:

      # Get data
      buffer = BytesIO()
      url = URL+"%d"%job_id
      response_code = curl_exec(url, buffer)

      # Manage HTTP success code
      # print(Fore.YELLOW + 'HTTP status code: %d' % response_code)
      if response_code == 200:
        parse_data(buffer)
        break

      # Skip the page if not exist
      if response_code == 404:
        break

      # Retry
      print(Fore.YELLOW + "Error: HTTP status code != 200")
      retry += 1
      if(retry > parameters.retry):
        print(Fore.RED + "End of query: error HTTP status code != 200")
        return -1

    job_id -= 1

  close_file()

  # Some stats
  print(Fore.GREEN + "Jobs count: ", jobs_count)
  print(Fore.GREEN + "Artifacts count: ", artifacts_count)
  print(Fore.GREEN + "Total size: ", sizeof_fmt(total_artifacts_size))  
  if artifacts_count !=0:
    print(Fore.GREEN + "Result saved in: " + outputfile)
  
  return 0

if __name__ == '__main__':
  sys.exit(main())