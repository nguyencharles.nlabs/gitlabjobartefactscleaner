import os, sys
import json
import colorama
from colorama import Fore

import parameters

jobs_count = 0
artifacts_count = 0
total_artifacts_size = 0
artifacts = []
outputfile = 'project-'+"%s"%parameters.project_id+'-artifacts.json'

def parse_jobjsonfile(file):
  global jobs_count
  global artifacts_count
  global total_artifacts_size
  global artifacts

  data = json.load(file)
  # print(json.dumps(data, indent=4))

  for i in range(len(data)):
    jobs_count += 1
    
    # Check if data is an array (or str)
    if isinstance(data, list):
      artifact_data = data[i].get('artifacts')
      
      if artifact_data != []:
        for j in range(len(artifact_data)):
          # Keep only artifact with the archive file_type
          if artifact_data[j].get('file_type') == "archive":
            artifact = {}
            artifact['id'] = data[i].get('id')
            if parameters.get_artifacts_with_url:
              artifact['url'] = data[i].get('web_url')
            if parameters.get_artifacts_with_date:
              artifact['date_job_creation'] = data[i].get('created_at')
              artifact['date_artifact_expiration'] = data[i].get('artifacts_expire_at')
            artifact['artifact'] = artifact_data[j]
            artifacts.append(artifact)
            total_artifacts_size += artifact_data[j]['size']
            artifacts_count+=1
  return 0

def check_if_result_exist():
  if os.path.isfile(outputfile):
    print(Fore.RED + "Error: " + outputfile + " already exist")
    return -1
  return 0

def keep_results():
  if check_if_result_exist() != 0:
    return -1
  ofile = open(outputfile,'w', encoding="utf8")
  ofile.write(json.dumps(artifacts))
  ofile.close()
  print(Fore.GREEN + "Result saved in: " + outputfile)
  return 0

def sizeof_fmt(num, suffix="B"):
  for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
    if abs(num) < 1024.0:
      return f"{num:3.1f}{unit}{suffix}"
    num /= 1024.0
  return f"{num:.1f}Yi{suffix}"

def main() -> int:
  colorama.init()

  if check_if_result_exist() != 0:
    return -1

  # Iterate for each project-<project_number>-jobs-page-<page_number>.json
  page = parameters.init_page
  while True:
    jobjsonfile = 'project-'+"%s"%parameters.project_id+'-jobs-page' + "%s"%page + '.json'
    if os.path.isfile(jobjsonfile):
      print(jobjsonfile)
      file = open(jobjsonfile,'r', encoding="utf8")
      parse_jobjsonfile(file)   # Parsing
      file.close()
    else:
      print(Fore.YELLOW + "no file: "+jobjsonfile)
      break
    page +=1
  
  # Some stats
  print(Fore.GREEN + "Jobs count: ", jobs_count)
  print(Fore.GREEN + "Artifacts count: ", artifacts_count)
  print(Fore.GREEN + "Total size: ", sizeof_fmt(total_artifacts_size))
  
  # Output
  if artifacts_count !=0:
    if keep_results() !=0:
      return -1
  
  return 0

if __name__ == '__main__':
  sys.exit(main())